# Recomendaciones para moderadores

Estas son unas notas para moderadores, que en parte pueden ser útiles también para los organizadores de salas.

## Proceso

15 minutos antes del comienzo de casa sesión, los ponentes deberían estar en la sala BBB correspondiente:

* Durante estos 15 minutos previos, el moderador se asegurará de que contacta con los (normalmente tres) ponentes. Con cada uno de ellos:

  - Le pasará a presentador, para que vea los nuevos menús que tendrá
  - Acordará si va a subir transpas, a compartir su pantalla, o a subir un vídeo
  - Si sube transparencias, las subirá y las probará. PDF es el formato preferido. LibreOffice seguramente funcionará también, PowerPoint quizás (el servidor las trata de convertir a formato LibreOffice). Ojo, que si tienen vídeos embebidos y animaciones puede que no funcionen.
  - Si sube transparencias, recordarle que le de al icono de compartirlas, para que la gente las pueda descargar (en el "+", opción de subir presentación, icono que sale para cada presentación en el listado de presentaciones subidas)
  - Si comparte pantalla, que tenga en cuenta que el framerate es como de un cuadro por segundo. No es adecuado para ver vídeos (si los pretende proyectar desde su pantalla), y no va muy bien con las animaciones de las presentaciones. Es muy posible que no se pueda compartir todo el escritorio desde Linux con Wayland (seguramente sí el navegador), con X Window va bien, es posible que no funcione algo en otros sistemas
  - Si quiere usar un vídeo, ha de estar subido a una de las plataformas soportadas (YouTube, Vimeo, Twitch, alguna otra), y probarlo. También, dar la url al moderador por si hubiera problemas.
  - El presentador probará el o los métodos que elija para su presentación

* Con todos ellos, comentará el esquema de tiempos (20 minutos para presentaciones normales, 10 para presentaciones rápidas, en ambos casos 5 minutos para preguntas y comentarios). Hará énfasis en que el tiempo será estricto, para evitar retrasos en el programa, y que los asistentes no puedan elegir presentaciones siguiendo el programa
* Mencionará los documentos básicos (de [ponente](recomendaciones-ponentes.md) y de [asistente](https://eslib.re/2020/asistentes) que tendrían que conocer

Cuando empiece la sesión:

* Comentario de que se va a usar el canal de RocketChat, y enlace al mismo en el canal de BBB con mensaje claro de que los comentarios van en ese canal (el de RocketChat)
* Seguir la checklist

Al empezar cada presentación:

* No olvidar pulsar el botón para iniciar la grabación
* Presentar al ponente (nombre, título de la charla)
* Pasar el modo presentador al ponente
* Si hay transparencias mencionar que se pueden descargar (recordar habérselo indicado al ponente, o hacerlo directamente con el permiso del ponente)

Al final de cada presentación:

* Avisar uno o dos minutos antes del final al ponente, por si acaso
* Avisar cuando queden como 15 o 30 segundos
* Al terminar el tiempo, quitar el modo presentación al ponente, e iniciar las preguntas
 No olvidar pulsar el botón para parar la grabación antes de iniciar las preguntas
* Dar turno a los que quieran hacer preguntas, procurar cortar preguntas que son más bien comentarios muy largos, recordar al presentador (si hace falta) que las respuestas sean breves. Recordar mirar en el canal de RocketChat por si hay preguntas por escrito (y animar a quien no pueda usar el micro que puede hacerlas allí)
* A los cinco minutos, terminar, recordar que quien quiera puede seguir en el canal de RocketChat

Al final de la sesión:

* Mencionar que se puede seguir hablando sobre la sesión en el canal de RocketChat
* Recordar que quien quiera puede invitar a otros a su sala personal ("Home Room" o "Sala Principal") para tener videoconferencia con quien quiera
* Mencionar que los ratos de descanso son especialmente interesantes para interaccionar con otros en RocketChat o en las salas personales
* Mencionar qué hay en el programa después del descanso, quizás compartiendo la pantalla para mostrar el programa
* Mencionar que esperamos subir las grabaciones en algunos días, que lo anunciaremos en el sitio web
* Comprobar la checklist

## Checklist al tratar con los ponentes antes de la sesión

* Enlace a la documentación para asistentes: https://eslib.re/2020/asistentes
* Enlace a la documentación para ponentes: https://gitlab.com/eslibre/docs/-/blob/master/recomendaciones-ponentes.md
* Elección entre subir transparencias, compartir pantalla y/o usar vídeo
* Pasarle a ponente y que vea las opciones de interfaz (según lo que ha elegido)
* Explicación de los tiempos, y de que son estrictos
* Si usa transpas, que las haya subido, las pruebe, y les de permisos para descarga
* Si usa vídeo, que el moderador tenga las urls, y las pruebe
* Si comparte pantalla, que lo pruebe con lo que vaya a usar en la presentación
* Que entienda y esté de acuerdo en que se le grabe
* Que entienda que se le pasará un formulario a firmar para dar permiso para la grabación

## Checklist al empezar la sesión

* Mención al canal en RocketChat y mensaje al respecto en el canal de BBB:
  "En esta sesión utilizaremos el canal de RocketChat <url>, y no este canal de BBB" (url será https://rocket.eslibre.urjc.es/channel/pista-i o similar)
* En el canal de RocketChat, enlace al canal de la pista en BBB:
  "Videoconferencia para esta pista: <url>" (la url se puede obtener del programa)
* Mención al programa, y enlace en canal de RocketChat
  "Programa de esLibre: https://eslib.re/2020/programa/"
* Mención al canal de ayuda, y enlace en el canal de RocketChat:
  "Canal de ayuda de esLibre: https://rocket.eslibre.urjc.es/channel/ayuda"
* Mención al uso de nombres de usuarios reales, para poder encontrarnos, o anónimos (en BBB) si no se quiere aparecer en las grabaciones
* Mención de que no se usen las cámaras durante la presentación, para mejorar el rendimiento
* Mención a que todo el mundo salvo el presentador esté silenciado, y silenciamiento a todo el mundo (opción en la configuración junto a la cabecera del listado de usuarios)
* Si el ponente usa transparencias, recordatorio que se pueden descargar, y mención del icono para hacerlo (esquina inferior izquierda)
* Mención de que el espacio para los vídeos (que aparecen en la parte superior) y para la presentación (que aparece en la parte inferior) se puede redimensionar: llevar el ratón a la línea imaginaria entre ambas zonas, cambiará la apariencia del puntero, arrastrar con el ratón.
* Descargar todas las transparencias, y enviarlas a Jesús González, con el asunto "esLibre: Transparencias de la Sesión de XX:XX a YY:YY de la Pista Z"

## En caso de problemas

Cualquier problema puede reportarse al canal [#ayuda](https://rocket.eslibre.urjc.es/channel/ayuda), donde trataremos de escuchar y ayudar. En caso de que haya habido un problema grave, y haya sido resuelto, reportadlo también en #ayuda, para poder detectar patrones de problemas.

En caso de que hay ruido en la sala, recordad que se puede usar la opción "Silenciar a todos menos al ponente" (menú de configuración en la cabecera de la lista de usuarios). Esto no impedirá que alguien se dessilencie, y el ruido persista. Si esto ocurriera, puede usarse, en el mismo menú, el bloqueo de asistentes: en el mismo menú, desplegará un nuevo menú donde se puede bloquear el audio de todos los asistentes (y estos no se podrán dessilenciar).

Si es preciso, se puede silenciar individualmente a un asistente (pulsar sobre el asistente en la lista de asistentes, aparecerá el menú que permite hacerlo).

En caso de que el rendimiento se degrade, se puede pedir a los asistentes que cierren sus cámaras, para que haya menos tráfico de vídeo (sólo hace estrictamente falta el del ponente). Si hay cámaras que no se cierran, y el rendimiento sigue degradado, se puede forzar, en el menú de bloqueo de asistentes (bloqueando las cámaras).

En casos extremos, se puede expulsar a un asistente (pulsar sobre el asistente en la lista de asistentes, aparecerá el menú que permite hacerlo), pedir en #ayuda que se habilite que sólo usuarios autenticados entren en la sala y/o pedir que se configure que el moderador tenga que admitir a cualquier nuevo asistente a la sala.

En cualquier caso, por favor, tomad medidas extremas sólo en casos extremos. Normalmente, cuando se explican los problemas, los asistentes los entenderán perfectamente y colaborarán.



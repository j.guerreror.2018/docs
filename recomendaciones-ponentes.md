# Recomendaciones para ponentes

Antes de leer este documento, por favor, leer el documento para asistentes a esLibre 2020, que detalla la estructura del congreso, y otra información que será de utilidad también para los ponentes (tanto de charlas como de talleres).

Una vez leído, quedan algunos detalles específicos para ponentes que se explican en este documento.

Recuerda que si tienes cualquier problema, puedes dirigirte al [canal de ayuda en RocketChat](https://rocket.eslibre.urjc.es/channel/ayuda).

## Preparación para tu charla o taller

Las sesiones "normales" son los periodos entre descansos, o entre sesiones plenarias y descansos, que ocurren en una pista determinada. Cada sesión "normal" está compuesta por bloques de dos o tres charlas. En el caso de los talleres, un taller ocupa una o varias sesiones. Normalmente, hay varias sesiones "normales" en paralelo.

Por lo tanto, lo primero es localizar en el programa tu sesión, cuándo empieza y cuándo termina, en qué pista tiene lugar, y en qué orden (y por lo tanto cuando empieza) tu charla. En el caso de los talleres, siempre empiezan al comienzo de la sesión.

Como cada pista tiene asignado un canal de RocketChat y un canal de BigBlueButton (BBB), tendrás que localizarlos también. Puedes hacerlo mirando en el programa: están enlazados en él con los iconos correspondientes.

Cada sesión tiene asignado un moderador. También tendrás que localizar al moderador de tu sesión. Procura presentarte a él localizándole en RocketChat y poniéndole un mensaje privado (y estate atento por si él te localiza primero). Para localizar tu moderador, puedes consultar la [asignación de moderadores](asignacion-moderadores.md)

En general, cada sesión de 75 minutos, con tres charlas. El moderador tendrá que ser estricto con los tiempos. Para cada charla normal, serán 20 de presentación (o menos) más 5 de preguntas (o más, hasta completar 25 desde el comienzo). Para las charlas cortas, serán 10 de presentación y 5 de preguntas. Los talleres, como ya se ha dicho, ocupan normalmente toda la sesión (pero es importante que empiecen y terminen a su hora, y que respeten los descansos, para que los asistentes se puedan mover de una sesión a otra según programa, puntualmente).

Cuando te crees cuenta en BBB, verás que tienes una sala especial para ti. Entra en ella, y prueba los diferentes elementos de la interfaz, y en particular todo lo que te haga falta para tu presentación (subir transparencias, compartir pantalla, poner un vídeo, poner la cámara, poner y quitar el micrófono, etc.)

## Quince minutos antes de que comience tu sesión

Ojo, que son quince minutos antes de que comience tu sesión, no tu charla.

Por favor, preséntate en el canal BBB de la pista que corresponda. Si llevas un taller, busca allí al coordinador técnico (que estará buscándote también), para resolver problemas técnicos y que puedas empezar en hora. Si presentas una charla, busca allí a tu moderador.

Con el coordinador técnico o tu moderador, podrás probar los elementos necesarios:

* Le informarás de si vas a usar transparencias, a compartir tu pantalla, o usar un vídeo
* Si usas transparencias, las subirás a BBB y las probarás. PDF es el formato preferido. LibreOffice seguramente funcionará también, PowerPoint quizás (el servidor las trata de convertir a formato LibreOffice). Ojo, que si tienen vídeos embebidos y animaciones puede que no funcionen. Si subes transparencias, puedes usar el icono de compartirlas, para que la gente las pueda descargar (en el "+", opción de subir presentación, icono que sale para cada presentación en el listado de presentaciones subidas).
* Si compartes pantalla, ten en cuenta que el régimen es como de un cuadro por segundo. No es adecuado para ver vídeos (si los pretendes proyectar desde tu pantalla), y no va muy bien con las animaciones de las presentaciones. Es muy posible que no se pueda compartir todo el escritorio desde Linux con Wayland (seguramente sí el navegador), con X Window va bien, es posible que no funcione algo en otros sistemas.
* Si quieres usar un vídeo, ha de estar subido a una de las plataformas soportadas (YouTube, Vimeo, Twitch, alguna otra). En este caso, da la url al moderador por si hubiera problemas.

Tendrás unos cinco minutos para probarlo todo, así que procura estar ya familiarizado con la interfaz de la aplicación, y ser puntual llegando esos 15 minutos antes de que comience la sesión.

En casos excepcionales, se admitirá vídeo en lugar de presentación en vivo para las charlas, y se recomienda grabarlo como plan B (por si hay problemas en la conectividad del ponente, por ejemplo). Que sea de menos de 20 minutos (o 10 en las charlas cortas). El vídeo habrá que subirlo a alguna de las plataformas soportadas (ver más abajo), y te asegurarás de que el moderador tenga el enlace antes de que comience la sesión. En cualquier caso, deberías estar presente para el periodo de preguntas después de tu charla. Si tuvieras problemas con el sonido, podrías contestar en el canal de RocketChat de tu pista (pero avisa al moderador). Los talleres tienen que ser en directo.

## Durante la charla / taller

En las charlas, el moderador te dará paso, y te cederá los permisos de presentador cuando te toque, según programa. En los talleres, tú mismo empezarás el taller y te presentarás (procura hacerlo puntualmente).

En general, el moderador se encargará de grabar las charlas. Si por algún motivo no quieres que la tuya se grabe, acláreselo al moderador. Más adelante te pediremos un permiso firmado para poderlas compartir. En los talleres, tendrás que coordinarte con el coordinador técnico para realizar la grabación, o hacerla tú directamente si prefieres seleccionar los momentos adecuados para grabar. En general, no grabes a asistentes (para evitar problemas con los permisos), graba sólo cuando tú estés hablando, y comenta que se va a grabar para que quien quiera pueda desactivar su cámara (algo que en general será una buena idea por motivos de rendimiento).

En el caso de las charlas, el moderador se encargará de leer el canal de RocketChat de la pista, para ver si hay preguntas y hacértelas en el momento adecuado. En el caso de los talleres, tendrás que avisar a tus asistentes de que usen ese canal, y tendrás que monitorizarlo para ver preguntas y comentarios.

## Después de tu charla / taller

Los comentarios pueden seguir en el canal de RocketChat de la pista correspondiente. Si quieres, también puedes invitar a seguir la discusión en tu sala personal de BBB (la que se crea cuando abres tu cuenta en BBB), y seguir allí en modo videoconferencia. También puedes quedar con otros para seguir la discusión durante uno de los descansos, en RocketChat o en BBB.

## Publicidad y redes sociales

Haced toda la publicidad que estiméis conveniente de vuestra propia presentación. Por favor, citad que es parte de esLibre, e incluid enlace a los datos de vuestra presentación o taller. Si usáis Twitter, mencionad a @esLibre_ para que podamos retuitearos. En general, usad el hashtag `#esLibre`.

## Evento social

Todos los asistentes, pero de forma muy especial los ponentes, están invitados a al evento social que tendrá lugar el viernes a las 19:00 en Mozilla Hubs, en unas salas personalizadas para esLibre. Los detalles se publicarán en el programa, a partir de las 15:00 del viernes.

## Código de conducta

Recordad que hay un esLibre se rige por un [Código de Conducta](https://eslib.re/2020/conducta/), que aceptaste cuando enviaste tu propuesta de charla o taller. Respétalo: es fácil, y todos salimos ganando.
